import { setDocumentCustomStyles, } from '@bbva-web-components/bbva-core-lit-helpers';
import { css, } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    margin: 0;
  }
  .container {
    width: calc(100vw - 30px);
    margin: 15px;
    overflow: auto;
  }
  h2 {
    font-size: 25px;
    text-align: center;
    font-weight: lighter;
    margin: 35px 0 10px 0;
  }
`);
